package de.oth.amp17.fhir.start.client;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import org.junit.Before;
import org.junit.Test;

public class ExplorationIT {

    private static final String SERVER_URL = "http://localhost:8090/baseDstu3";

    private FhirContext context;

    private IGenericClient client;

    @Before
    public void setup() {
        context = FhirContext.forDstu3();
        client = context.newRestfulGenericClient(SERVER_URL);
    }

    @Test
    public void create_patient() {

    }

    @Test
    public void create_observation() {

    }

}
