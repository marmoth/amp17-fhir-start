package de.oth.amp17.fhir.start.process.boundary;


import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import org.hl7.fhir.dstu3.model.Observation;
import org.hl7.fhir.dstu3.model.Quantity;
import org.hl7.fhir.exceptions.FHIRException;
import org.hl7.fhir.instance.model.api.IBaseResource;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;

@Path("/fhir/inbound/{type}/{id}")
@Consumes({MediaType.APPLICATION_JSON, "application/fhir+json"})
public class SubscriptionTargetResource {

    private static final Logger LOG = Logger.getLogger(SubscriptionTargetResource.class.getName());

    @PUT
    public void handleResource(
            @PathParam("type") String type,
            @PathParam("id") String id,
            String payload) throws FHIRException {
        FhirContext ctx = FhirContext.forDstu3();
        IParser parser = ctx.newJsonParser();
        IBaseResource resource = parser.parseResource(payload);

        LOG.log(Level.INFO, "RECEIVED :: {0}/{1}\n\t{2}\n\t{3}", new Object[]{
                type, id, resource.getIdElement().toString(), payload});

        if (!"Observation".equals(type)) {
            return;
        }

        Observation observation = (Observation) resource;

        Quantity quantity = observation.getValueQuantity();
        BigDecimal value = quantity.getValue();
        String unit = quantity.getUnit();
        LOG.log(Level.INFO, "RECEIVED :: Observation :: {0} {1}",
                new Object[] {value, unit}
        );

    }

}
